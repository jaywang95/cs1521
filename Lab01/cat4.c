// Copy input to output
// COMP1521 18s1

#include <stdlib.h>
#include <stdio.h>

void copy(FILE *, FILE *);

int main(int argc, char *argv[])
{
    FILE* fp2 = fopen("test1.txt", "w");
    for (int i = 1; i < argc; i++) {
        if (argc == 1) {
            copy(stdin,stdout);
        } else {
            FILE* fp = fopen(argv[i], "r");
            if (fp == NULL) {
                printf("Can't read NameOfFile \n");
            } else {
                copy(fp, fp2);
                fputs("\n", fp2);
                fclose(fp);
            }
        }
    }
    fclose(fp2);

	return EXIT_SUCCESS;
}

// Copy contents of input to output, char-by-char
// Assumes both files open in appropriate mode

void copy(FILE *input, FILE *output)
{
    char str[BUFSIZ];
    while (fgets(str, BUFSIZ, input) != NULL) {
        fputs(str,output);
    }
}
